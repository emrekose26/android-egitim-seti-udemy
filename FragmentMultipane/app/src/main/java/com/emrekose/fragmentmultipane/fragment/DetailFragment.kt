package com.emrekose.fragmentmultipane.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.emrekose.fragmentmultipane.R

class DetailFragment : Fragment() {

    companion object {
        fun newInstance(name: String): DetailFragment {
            val args = Bundle()
            args.putString("name", name)

            val fragment = DetailFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_detail, container, false)

        val name: String? = arguments?.getString("name")

        view.findViewById<TextView>(R.id.detail_name).text = name

        return view
    }

}
