package com.emrekose.fragmentmultipane.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.FrameLayout
import com.emrekose.fragmentmultipane.R
import com.emrekose.fragmentmultipane.fragment.DetailFragment
import com.emrekose.fragmentmultipane.fragment.ListFragment

class MainActivity : AppCompatActivity(), ListFragment.OnListItemClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, ListFragment())
            .commit()
    }

    override fun onItemClick(name: String) {
        if (findViewById<FrameLayout>(R.id.detail_container) != null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.detail_container, DetailFragment.newInstance(name))
                .commit()
        } else {
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra("username", name)
            startActivity(intent)
        }
    }
}
