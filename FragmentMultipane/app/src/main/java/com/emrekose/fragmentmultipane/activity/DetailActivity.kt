package com.emrekose.fragmentmultipane.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.emrekose.fragmentmultipane.R
import com.emrekose.fragmentmultipane.fragment.DetailFragment

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val username: String = intent.extras.getString("username")

        supportFragmentManager.beginTransaction()
            .replace(R.id.detail_container, DetailFragment.newInstance(username))
            .commit()
    }
}
