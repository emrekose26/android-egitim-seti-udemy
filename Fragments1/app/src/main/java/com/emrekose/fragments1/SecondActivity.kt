package com.emrekose.fragments1

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val firstFragment = FirstFragment()
        supportFragmentManager.beginTransaction()
            .add(R.id.container, firstFragment)
            .commit()


        val secondFragment = SecondFragment()
        btn_add.setOnClickListener{

            supportFragmentManager.beginTransaction()
                .replace(R.id.container, secondFragment)
                .commit()
        }

        btn_remove.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .remove(secondFragment)
                .commit()
        }
    }
}
