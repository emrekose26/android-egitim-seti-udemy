package com.emrekose.todolistapp.ui.activity

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Toast
import com.emrekose.todolistapp.R
import com.emrekose.todolistapp.db.TaskRepository
import com.emrekose.todolistapp.model.Task
import kotlinx.android.synthetic.main.activity_task.*
import java.util.*

class TaskActivity : AppCompatActivity() {

    private lateinit var taskRepository: TaskRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)

        setSupportActionBar(task_toolbar)
        supportActionBar?.title = "Add Todo"

        taskRepository = TaskRepository(this)

        if (intent.extras != null) {
            val task: Task = intent.extras.getSerializable(MainActivity.EXTRA_TASK) as Task
            task_name_edt.setText(task.name)
            end_date_text.text = task.date
        }

        confirm_fab.setOnClickListener {

            if(intent.extras != null) {
                val task: Task = intent.extras.getSerializable(MainActivity.EXTRA_TASK) as Task
                val rowId = taskRepository.updateTask(Task(task.id, task_name_edt.text.toString(), end_date_text.text.toString()))
                if (rowId > -1) Toast.makeText(this, "Güncellendi", Toast.LENGTH_SHORT).show()
                else Toast.makeText(this, "Güncelleme Başarısız", Toast.LENGTH_SHORT).show()
            } else {
                if (!TextUtils.isEmpty(task_name_edt.text.toString())) {
                    val date: String =
                        if (end_date_text.text == null || end_date_text.text == getString(R.string.end_date)) "No end date"
                        else end_date_text.text.toString()

                    val rowId = taskRepository.insertTask(Task(name = task_name_edt.text.toString(), date = date))

                    if (rowId > -1) Toast.makeText(this, "Eklendi", Toast.LENGTH_SHORT).show()
                    else Toast.makeText(this, "Ekleme başarısız", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Task adı boş geçilemez", Toast.LENGTH_SHORT).show()
                }
            }
        }
        end_date_layout.setOnClickListener { getDatePickerDialog() }
    }

    private fun getDatePickerDialog() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dialog = DatePickerDialog(this, { view, year, month, dayOfMonth ->
            val endDate = "$dayOfMonth.$month.$year" // 19.01.2018
            end_date_text.text = endDate
        }, year, month, day)

        dialog.datePicker.minDate = System.currentTimeMillis()
        dialog.show()
    }
}
