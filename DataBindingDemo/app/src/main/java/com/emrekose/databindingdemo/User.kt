package com.emrekose.databindingdemo

data class User(var name: String, var age: Int)