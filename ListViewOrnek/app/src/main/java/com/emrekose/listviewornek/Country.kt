package com.emrekose.listviewornek

data class Country(var name: String, var flag: Int)