package com.emrekose.listviewornek

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_custom_list_view.*

class CustomListViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_list_view)

        var countryList: ArrayList<Country> = ArrayList()
        countryList.add(Country("Türkiye",R.drawable.turkey))
        countryList.add(Country("Brezilya",R.drawable.brazil))
        countryList.add(Country("Bulgaristan",R.drawable.bulgaria))
        countryList.add(Country("Çin",R.drawable.china))
        countryList.add(Country("Hırvastistan",R.drawable.croatia))
        countryList.add(Country("İngiltere",R.drawable.england))
        countryList.add(Country("Macaristan",R.drawable.hungary))
        countryList.add(Country("Japonya",R.drawable.japan))
        countryList.add(Country("Meksika",R.drawable.mexico))
        countryList.add(Country("Norveç",R.drawable.norway))
        countryList.add(Country("Romanya",R.drawable.romania))
        countryList.add(Country("Rusya",R.drawable.russia))
        countryList.add(Country("İsveç",R.drawable.sweden))


        country_listview.adapter = CountryAdapter(this, countryList)
    }
}
