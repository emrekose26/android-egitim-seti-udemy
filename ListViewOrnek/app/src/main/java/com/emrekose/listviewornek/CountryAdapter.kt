package com.emrekose.listviewornek

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class CountryAdapter(var context: Context, var countryList: ArrayList<Country>): BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view: View?
        var viewHolder: ViewHolder

        if(convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_country_list, parent, false)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        var country: Country = getItem(position) as Country
        viewHolder.countryNameTv?.text = country.name
        viewHolder.flagImg?.setImageResource(country.flag)

        return view as View
    }

    override fun getItem(position: Int): Any = countryList[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = countryList.size

    private class ViewHolder(view: View?) {
        var countryNameTv: TextView?
        var flagImg: ImageView?

        init {
            countryNameTv = view?.findViewById<TextView>(R.id.country_name)
            flagImg = view?.findViewById<ImageView>(R.id.country_flag)
        }
    }
}