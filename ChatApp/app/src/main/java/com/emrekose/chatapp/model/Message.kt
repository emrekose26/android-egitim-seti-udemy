package com.emrekose.chatapp.model

data class Message(
    var message: String = "",
    var time: String = "",
    var from: String = ""
)