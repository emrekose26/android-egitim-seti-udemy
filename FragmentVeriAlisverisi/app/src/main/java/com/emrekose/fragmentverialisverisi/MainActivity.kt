package com.emrekose.fragmentverialisverisi

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), FirstFragmentListener {

    private var firstFragment: FirstFragment? = null
    private var secondFragment: SecondFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        firstFragment = FirstFragment()
        secondFragment = SecondFragment()

        supportFragmentManager.beginTransaction()
            .replace(R.id.first_container, firstFragment!!)
            .commit()

        supportFragmentManager.beginTransaction()
            .replace(R.id.second_container, secondFragment!!)
            .commit()
    }

    override fun messageFromFirst(message: String) {
        secondFragment?.setMessage(message)
        fta_tv.text = message
    }
}
