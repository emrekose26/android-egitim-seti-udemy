package com.emrekose.appbarcollapsing

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun scrollFlagOnClick(view: View) {
        startActivity(Intent(this, ScrollFlagActivity::class.java))
    }

    fun enterAlwaysFlagOnClick(view: View) {
        startActivity(Intent(this, EnterAlwaysFlagActivity::class.java))
    }

    fun enterAlwaysCollapsedFlagOnClick(view: View) {
        startActivity(Intent(this, EnterAlwaysCollapsedActivity::class.java))
    }

    fun exitUntilCollapsedFlagOnClick(view: View) {
        startActivity(Intent(this, ExitUntilCollapsedActivity::class.java))
    }

    fun snapFlagOnClick(view: View){
        startActivity(Intent(this, SnapFlagActivity::class.java))
    }

}
