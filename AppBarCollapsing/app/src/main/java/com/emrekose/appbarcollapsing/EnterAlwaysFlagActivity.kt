package com.emrekose.appbarcollapsing

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_enter_always_flag.*

class EnterAlwaysFlagActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_always_flag)

        setSupportActionBar(toolbar)
        toolbar.title = "Scroll Flag"
    }
}
