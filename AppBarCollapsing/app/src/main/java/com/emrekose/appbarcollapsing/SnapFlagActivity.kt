package com.emrekose.appbarcollapsing

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_snap_flag.*

class SnapFlagActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_snap_flag)

        setSupportActionBar(toolbar)
        toolbar.title = "Snap Flag"
    }
}
