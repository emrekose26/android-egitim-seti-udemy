package com.emrekose.activityler

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        var isimIntent = getIntent()
        //var isim = isimIntent.getStringExtra(Constants.EXTRA_NAME)

        //hosgeldiniz_tv.text = "Hoşgeldiniz $isim"

        var extras: Bundle = isimIntent.extras
        var isim: String = extras.getString(Constants.EXTRA_NAME)

        hosgeldiniz_tv.text = "Hoşgeldiniz $isim"

        button.setOnClickListener { haritadaGoster() }
        button2.setOnClickListener { webSayfasiAc() }
        button3.setOnClickListener { paylas() }
    }

    fun haritadaGoster() {
        val location = Uri.parse("geo:37.7749,-122.4194")
        val mapIntent = Intent(Intent.ACTION_VIEW, location)
        if(mapIntent.resolveActivity(packageManager) != null) {
            startActivity(mapIntent)
        }
    }

    fun webSayfasiAc() {
        val webpage = Uri.parse("http://google.com")
        val webIntent = Intent(Intent.ACTION_VIEW, webpage)
        if(webIntent.resolveActivity(packageManager) != null) {
            startActivity(webIntent)
        }
    }

    fun paylas() {
        val paylasIntent = Intent()
        paylasIntent.setAction(Intent.ACTION_SEND)
        paylasIntent.putExtra(Intent.EXTRA_TEXT, "Uygulamayı indirmek için http://play....")
        paylasIntent.setType("text/plain")
        startActivity(paylasIntent)
    }
























}
