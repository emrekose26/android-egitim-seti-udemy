package com.emrekose.retrofitgsondemo

import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    // https://jsonplaceholder.typicode.com/posts
    @GET("posts")
    fun getPostList(): Call<List<Post>>

    // https://jsonplaceholder.typicode.com/posts/10
    @GET("posts/{id}")
    fun getSinglePost(@Path("id") postId: Int): Call<Post>

    // https://jsonplaceholder.typicode.com/posts?userId=10
    // https://jsonplaceholder.typicode.com/posts?userId=10&id=90
    @GET("posts")
    fun getPostwithQuery(@Query("userId") userId: Int): Call<List<Post>>

    @POST("posts")
    @FormUrlEncoded
    fun savePost(@Body post: Post): Call<Post>

    @POST("posts")
    @FormUrlEncoded
    fun savePostwithField(@Field("title") title: String, @Field("body") body: String): Call<Post>


    @PUT("posts/{id}")
    @FormUrlEncoded
    fun updatePost(@Path("id") postId: Int, @Field("title") title: String, @Field("body") body: String): Call<Post>

    @DELETE("posts/{id}")
    fun deletePost(@Path("id") postId: Int): Call<Post>

}