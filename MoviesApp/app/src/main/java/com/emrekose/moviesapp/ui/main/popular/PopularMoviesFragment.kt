package com.emrekose.moviesapp.ui.main.popular

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.emrekose.moviesapp.R
import com.emrekose.moviesapp.common.BaseVMFragment
import com.emrekose.moviesapp.model.movie.MovieResults
import com.emrekose.moviesapp.ui.detail.DetailActivity
import com.emrekose.moviesapp.ui.main.MovieAdapter
import com.emrekose.moviesapp.util.Constants
import com.emrekose.moviesapp.util.gone
import com.emrekose.moviesapp.util.visible
import kotlinx.android.synthetic.main.fragment_popular_movies.*

class PopularMoviesFragment: BaseVMFragment<PopularMoviesViewModel>(), MovieAdapter.OnMovieClickListener {

    private lateinit var adapter: MovieAdapter

    override fun getViewModel(): Class<PopularMoviesViewModel> = PopularMoviesViewModel::class.java

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_popular_movies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = MovieAdapter()
        popular_recyclerview.layoutManager = GridLayoutManager(activity, 2)

        adapter.setOnMovieClickListener(this)

        viewModel.getPopularMovies()?.observe(this, Observer {
            adapter.submitList(it)
            popular_recyclerview.adapter = adapter
            popular_recyclerview.visible()
            popular_progressbar.gone()
        })
    }


    override fun onMovieClick(movieResults: MovieResults) {
        val intent = Intent(activity, DetailActivity::class.java)
        intent.putExtra(Constants.EXTRA_MOVIES, movieResults)
        startActivity(intent)
    }
}