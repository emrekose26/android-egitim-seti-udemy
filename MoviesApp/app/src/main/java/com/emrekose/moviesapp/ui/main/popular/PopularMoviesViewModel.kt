package com.emrekose.moviesapp.ui.main.popular

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.emrekose.moviesapp.model.movie.MovieResults
import com.emrekose.moviesapp.ui.main.MainRepository

class PopularMoviesViewModel: ViewModel() {
    private val repository: MainRepository by lazy { MainRepository() }

    fun getPopularMovies(): LiveData<List<MovieResults>>? = repository.getPopularMovies()
}