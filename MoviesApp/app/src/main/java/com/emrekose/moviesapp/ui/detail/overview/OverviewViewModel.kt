package com.emrekose.moviesapp.ui.detail.overview

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.emrekose.moviesapp.model.detail.MovieDetailResponse
import com.emrekose.moviesapp.model.videos.MovieVideoResults

class OverviewViewModel: ViewModel() {

    private val repository: OverviewRepository by lazy { OverviewRepository() }

    fun getDetails(movieId: Int): LiveData<MovieDetailResponse> = repository.getDetails(movieId)

    fun getMovieVideos(movieId: Int): LiveData<List<MovieVideoResults>> = repository.getMovieVideos(movieId)
}