package com.emrekose.moviesapp.data.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.emrekose.moviesapp.model.movie.MovieResults

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovie(movie: MovieResults?)

    @Delete
    fun deleteMovie(movie: MovieResults?)

    @Query("SELECT * FROM movies")
    fun getAllMovies(): LiveData<List<MovieResults>>

    @Query("SELECT * FROM movies WHERE movieId= :movieId")
    fun getSingleMovie(movieId: Int?): LiveData<MovieResults>
}