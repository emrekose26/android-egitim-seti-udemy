package com.emrekose.spinnerornek

data class Country(var name: String, var flag: Int)