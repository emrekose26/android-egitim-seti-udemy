package com.emrekose.viewmodeldemo

import android.arch.lifecycle.ViewModel

class CounterViewModel: ViewModel() {
    var counterValue: Int = 0
}