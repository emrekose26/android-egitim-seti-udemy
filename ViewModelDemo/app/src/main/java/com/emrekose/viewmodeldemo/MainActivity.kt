package com.emrekose.viewmodeldemo

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: CounterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(CounterViewModel::class.java)

        btn_increase.setOnClickListener { increaseValue() }

        counter_text.text = viewModel.counterValue.toString()
    }

    private fun increaseValue() {
        viewModel.counterValue += 1
        counter_text.text = viewModel.counterValue.toString()
    }

    /*
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putInt("counter_val", counterValue)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        savedInstanceState?.let {
            counterValue = it.getInt("counter_val")
            counter_text.text = counterValue.toString()
        }
    }
    */
}

