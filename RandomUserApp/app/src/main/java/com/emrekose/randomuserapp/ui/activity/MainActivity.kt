package com.emrekose.randomuserapp.ui.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.widget.ImageView
import com.emrekose.randomuserapp.R
import com.emrekose.randomuserapp.data.ApiClient
import com.emrekose.randomuserapp.model.Results
import com.emrekose.randomuserapp.model.UserResponse
import com.emrekose.randomuserapp.ui.adapter.UserAdapter
import com.emrekose.randomuserapp.util.gone
import com.emrekose.randomuserapp.util.visible
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), UserAdapter.OnUserClickListener {

    private val apiClient by lazy { ApiClient.getApiClient() }
    private lateinit var adapter: UserAdapter

    companion object {
        const val EXTRA_RESULT_ITEM = "extra_result_item"
        const val EXTRA_RESULT_TRANSITION_NAME = "extra_transition_name"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerview.layoutManager = LinearLayoutManager(this)
        recyclerview.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        progressBar.visible()
        recyclerview.gone()
        getUsers()

        swipe_refresh_layout.setOnRefreshListener {
            getUsers()
        }
    }

    private fun getUsers() {
        apiClient.getUsers(10).enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                Log.e("MainActivity", t.message)
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                if (response.isSuccessful) {
                    adapter = UserAdapter(response.body()?.userList!!, this@MainActivity)
                    recyclerview.adapter = adapter
                    progressBar.gone()
                    recyclerview.visible()
                    swipe_refresh_layout.isRefreshing = false
                }
            }

        })
    }

    override fun onUserClick(position: Int, results: Results, sharedImageView: ImageView) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(EXTRA_RESULT_ITEM, results)
        intent.putExtra(EXTRA_RESULT_TRANSITION_NAME, ViewCompat.getTransitionName(sharedImageView))

        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            this,
            sharedImageView,
            ViewCompat.getTransitionName(sharedImageView)!!
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            startActivity(intent, options.toBundle())
        } else {
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val searchItem = menu?.findItem(R.id.action_search)
        val searchView: SearchView = searchItem?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                adapter.getFilter().filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                // Log.e("onQueryTextChange", newText)
                adapter.getFilter().filter(newText)
                return false
            }

        })

        return true
    }
}
