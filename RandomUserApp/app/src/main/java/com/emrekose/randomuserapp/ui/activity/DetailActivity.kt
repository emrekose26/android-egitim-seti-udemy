package com.emrekose.randomuserapp.ui.activity

import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.emrekose.randomuserapp.R
import com.emrekose.randomuserapp.model.Results
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*


class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        supportPostponeEnterTransition()

        val results: Results = intent.extras.getSerializable(MainActivity.EXTRA_RESULT_ITEM) as Results

        detail_username_tv.text = "${results.name.first} ${results.name.last}"
        detail_email_tv.text = results.email
        detail_user_adress_tv.text = "${results.location.street} ${results.location.city}"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val imageTransitionName = intent.extras.getString(MainActivity.EXTRA_RESULT_TRANSITION_NAME)
            detail_image.transitionName = imageTransitionName
        }

        Picasso.get()
            .load(results.picture.large)
            .into(detail_image, object: Callback {
                override fun onSuccess() {
                    supportStartPostponedEnterTransition()
                }

                override fun onError(e: Exception?) {
                    supportStartPostponedEnterTransition()
                }
            })

    }
}
