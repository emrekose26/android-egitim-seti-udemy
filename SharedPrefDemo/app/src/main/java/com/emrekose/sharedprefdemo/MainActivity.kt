package com.emrekose.sharedprefdemo

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var sharedPref: SharedPreferences
    private lateinit var prefs: SharedPreferences

    companion object {
        val PREFERENCE_FILE_KEY = "my_pref_key"
        val EDT_KEY = "edt_key"
        val CHK_KEY = "chk_key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sharedPref = getSharedPreferences(PREFERENCE_FILE_KEY, Context.MODE_PRIVATE)
        prefs = getPreferences(Context.MODE_PRIVATE)

        readValues()
        btn_save.setOnClickListener {
            saveEdtValue()
            saveCheckBoxValue()
        }
    }

    private fun saveEdtValue() {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.apply {
            putString(EDT_KEY, editText.text.toString())
            apply()
        }
    }

    private fun saveCheckBoxValue() {
        val editor : SharedPreferences.Editor = prefs.edit()
        editor.apply {
            putBoolean(CHK_KEY, checkBox.isChecked)
            apply()
        }
    }

    private fun readValues() {
        val value = sharedPref.getString(EDT_KEY, "none")
        textView.text = value

        val chkValue = prefs.getBoolean(CHK_KEY, false)
        checkBox.isChecked = chkValue
    }










}
