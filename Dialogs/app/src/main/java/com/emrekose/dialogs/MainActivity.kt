package com.emrekose.dialogs

import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_alert_dialog.setOnClickListener{
            val builder: AlertDialog.Builder = AlertDialog.Builder(this@MainActivity)
            builder.setMessage("Çıkış yapılsın mı?")
                .setTitle("Çıkış")
                .setPositiveButton("Evet") { dialog, which ->
                    Toast.makeText(this, "Evet", Toast.LENGTH_SHORT).show()
                }
                .setNegativeButton("Hayır") { dialog, which ->
                    Toast.makeText(this, "Hayır", Toast.LENGTH_SHORT).show()
                }


            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

        btn_list_alert.setOnClickListener {
            val builder = AlertDialog.Builder(this@MainActivity)
            builder.setTitle("Renk Seçiniz")
                .setItems(R.array.colors, DialogInterface.OnClickListener{ dialog, which ->

                })

            builder.create().show()
        }

        btn_custom_dialog.setOnClickListener {
            val customDialog: CustomDialog = CustomDialog()
            customDialog.show(supportFragmentManager, "custom_dialog")
        }
    }
}


























