package com.emrekose.snackbarandfab

import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val cl = findViewById<CoordinatorLayout>(R.id.coordinatorLayout)

        btn_snackbar.setOnClickListener {
            val snackbar = Snackbar.make(cl, "Mesaj silindi", Snackbar.LENGTH_LONG)
                .setAction("Geri Al") { view ->
                    val snackbar2 = Snackbar.make(cl, "Mesaj silinmedi", Snackbar.LENGTH_LONG).show()
                }

            snackbar.show()
        }
    }
}
