package com.emrekose.recyclerviewdemo

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class UserRecyclerViewAdapter(var userList: ArrayList<User>, var userClickListener: OnUserClickListener):
    RecyclerView.Adapter<UserRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.user_item_cardview, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = userList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.username.text = userList[position].name
        holder.userimg.setImageResource(userList[position].image)
        holder.itemView.setOnClickListener {
            userClickListener.onUserClick(userList[position])
        }
    }


    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val username = view.findViewById<TextView>(R.id.username_tv)
        val userimg = view.findViewById<ImageView>(R.id.user_img)
    }
}