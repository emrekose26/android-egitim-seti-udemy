package com.emrekose.recyclerviewdemo

interface OnUserClickListener {
    fun onUserClick(user: User)
}