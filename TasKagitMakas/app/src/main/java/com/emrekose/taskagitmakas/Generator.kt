package com.emrekose.taskagitmakas

import java.util.*

object Generator {

    fun generateRandomNumber(): Int = Random().nextInt(3) // 0,1,2

    fun getImage(): Int {
        val images = listOf(R.drawable.tas, R.drawable.kagit, R.drawable.makas)
        return images[generateRandomNumber()]
    }
}