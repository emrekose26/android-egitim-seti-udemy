package com.emrekose.fragmentsdemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val firstFragment = BlankFragment()
        supportFragmentManager.beginTransaction()
                .add(R.id.container, firstFragment)
                .commit()

        button.setOnClickListener {
            val secondFragment = SecondFragment()
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, secondFragment)
                    .commit()
        }
    }
}
