package com.emrekose.quizapp.ui.start

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.emrekose.quizapp.R
import com.emrekose.quizapp.ui.add.AddQuestionActivity
import com.emrekose.quizapp.ui.quiz.QuizActivity
import kotlinx.android.synthetic.main.activity_start.*

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        start_quiz_btn.setOnClickListener {
            startActivity(Intent(this, QuizActivity::class.java))
            finish()
        }

        add_question_btn.setOnClickListener {
            startActivity(Intent(this, AddQuestionActivity::class.java))
        }
    }
}
