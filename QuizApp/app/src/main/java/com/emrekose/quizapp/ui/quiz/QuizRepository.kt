package com.emrekose.quizapp.ui.quiz

import android.content.Context
import androidx.lifecycle.LiveData
import com.emrekose.quizapp.data.QuestionDao
import com.emrekose.quizapp.data.QuestionDb
import com.emrekose.quizapp.model.Question

class QuizRepository(context: Context) {
    private val db by lazy { QuestionDb.getInstance(context) }
    private val dao: QuestionDao by lazy { db.questionDao() }

    fun getAllQuestions(): LiveData<List<Question>> =
            dao.getQuestions()
}