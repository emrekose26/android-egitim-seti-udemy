package com.emrekose.quizapp.ui.result

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.emrekose.quizapp.R
import com.emrekose.quizapp.util.Constants
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        intent.extras.let {
            val result = intent.extras.getInt(Constants.EXTRA_RESULT)
            val listSize = intent.extras.getInt(Constants.EXTRA_LIST_SIZE)
            result_tv.text = "$listSize sorudan $result tanesini doğru bildiniz."
        }
    }
}
