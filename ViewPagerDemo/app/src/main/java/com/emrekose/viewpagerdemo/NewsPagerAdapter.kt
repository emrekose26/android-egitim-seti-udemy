package com.emrekose.viewpagerdemo

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class NewsPagerAdapter(var context: Context, var newsList: ArrayList<News>): PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val news = newsList[position]
        val view: View = LayoutInflater.from(context).inflate(R.layout.view_news, container, false)

        val title: TextView = view.findViewById(R.id.news_title)
        val content: TextView = view.findViewById(R.id.news_content)

        title.text = news.title
        content.text = news.content

        view.tag = news
        container.addView(view)

        return view
    }

    override fun getCount(): Int = newsList.size

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any)  =
        container.removeView(obj as View)

    override fun isViewFromObject(view: View, obj: Any): Boolean = (view == obj)
}