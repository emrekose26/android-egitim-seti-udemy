package com.emrekose.viewpagerdemo

data class News(var title: String, var content: String)