package com.emrekose.bottomsheetdemo

import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.bottom_sheet.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var bsBehavior: BottomSheetBehavior<LinearLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bsBehavior = BottomSheetBehavior.from(bottom_sheet)

        btn_bottom_sheet.setOnClickListener {
            if(bsBehavior.state != BottomSheetBehavior.STATE_EXPANDED)
                bsBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            else
                bsBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }

        btn_bottom_sheet_frg.setOnClickListener {
            BottomSheetFragment().apply {
                show(supportFragmentManager, this.tag)
            }
        }


        btn_bottom_sheet_dialog.setOnClickListener {
            val view: View = layoutInflater.inflate(R.layout.fragment_bottom_sheet, null)

            val dialog = BottomSheetDialog(this)
            dialog.apply {
                setContentView(view)
                show()
            }
        }
    }
}
