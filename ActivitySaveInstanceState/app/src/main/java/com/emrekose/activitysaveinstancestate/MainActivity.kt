package com.emrekose.activitysaveinstancestate

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var counterValue: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_increase.setOnClickListener { increaseValue() }
    }

    private fun increaseValue() {
        counterValue += 1
        counter_text.text = counterValue.toString()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putInt("counter_val", counterValue)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        savedInstanceState?.let {
            counterValue = it.getInt("counter_val")
            counter_text.text = counterValue.toString()
        }
    }
}
