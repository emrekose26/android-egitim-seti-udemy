package com.emrekose.uielemanlari

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.RadioButton
import android.widget.Toast

class SecondActivity : AppCompatActivity() {

    private lateinit var button: Button
    private lateinit var macosCB: CheckBox
    private lateinit var windowsCB: CheckBox
    private lateinit var linuxCB: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        button = findViewById(R.id.button_secim)
        macosCB = this.findViewById(R.id.macos_cb)
        windowsCB = findViewById(R.id.windows_cb)
        linuxCB = findViewById(R.id.linux_cb)

        button.setOnClickListener {
            var result = "Selected OS"
            if(macosCB.isChecked) {
                result += "\nmacOs"
            }
            if(windowsCB.isChecked) {
                result += "\nWindows"
            }
            if(linuxCB.isChecked) {
                result += "\nLinux"
            }

            Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
        }
    }

    fun onCheckbxClicked(view: View) {
        if(view is CheckBox) {
            when(view.id) {
                R.id.macos_cb -> TODO()
                R.id.windows_cb -> TODO()
                R.id.linux_cb -> TODO()
            }
        }
    }

    fun onRadioButtonClicked(view: View) {
        if(view is RadioButton) {
            val checked: Boolean = view.isChecked

            when(view.id) {
                R.id.radioButton -> {
                    if (checked) Toast.makeText(this, "Erkek", Toast.LENGTH_SHORT).show()
                }
                R.id.radioButton2 -> {
                    if(checked) Toast.makeText(this, "Kadın", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}
